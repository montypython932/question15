import javax.swing.JOptionPane;
public class Question15
{
	public static void main(String [] args)
	{
		String input = JOptionPane.showInputDialog("Enter a date in the format MM/DD/YY");
		String month = input.substring(0, 2);
		String day = input.substring(3, 5);
		String year = input.substring( 6, 8);
		JOptionPane.showMessageDialog(null, day + "." + month + "." + year);
		System.exit(0);
	}
}